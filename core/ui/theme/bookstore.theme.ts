import { ThemeOptions } from "@mui/material";
import cssBaselineOverrides from "./overrides/cssBaseline.override";
import globalTypography from "./typography/global.typography";

const bookstoreTheme: ThemeOptions = {
  typography: globalTypography,
  components: { MuiCssBaseline: cssBaselineOverrides },
};

export default bookstoreTheme;
