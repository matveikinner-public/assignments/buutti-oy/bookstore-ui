import { all } from "redux-saga/effects";
import bookstoreRootSagas from "@bookstore/ui/adapters/redux-saga";

export default function* rootSaga() {
  yield all([bookstoreRootSagas()]);
}
