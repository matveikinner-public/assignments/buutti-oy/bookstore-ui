import { combineReducers } from "redux";
import loaderReducer from "./loader/loader.reducer";
import toastReducer from "./toast/toast.reducer";

export default combineReducers({
  loader: loaderReducer,
  toast: toastReducer,
});
