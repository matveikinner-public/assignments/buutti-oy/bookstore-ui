import { RouteProps } from "react-router-dom";
import BookStore from "@bookstore/ui/app/BookStore";

const routes: RouteProps[] = [
  {
    path: "/",
    exact: false,
    children: BookStore,
  },
];

export default routes;
