import React, { FunctionComponent } from "react";
import { Provider } from "react-redux";
import { Switch, Route, Redirect } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router/immutable";
import { Container, createTheme, CssBaseline, ThemeProvider } from "@mui/material";
import { v4 as uuid } from "uuid";
import store, { history } from "../frameworks/redux.config";
import routes from "./App.routes";
import bookstoreTheme from "../theme/bookstore.theme";

const App: FunctionComponent = () => {
  const theme = createTheme({ ...bookstoreTheme });
  return (
    <Provider store={store}>
      <CssBaseline />
      <ThemeProvider theme={theme}>
        <Container
          maxWidth='lg'
          sx={{
            height: "100vh",
            py: 7,
            px: 2,
            pb: 2,
            [theme.breakpoints.up("sm")]: {
              py: 8,
              px: 4,
              pb: 4,
            },
          }}
        >
          <ConnectedRouter history={history}>
            <Switch>
              {routes.map((route) => (
                <Route key={uuid()} {...route} />
              ))}
            </Switch>
            <Redirect to='/' />
          </ConnectedRouter>
        </Container>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
