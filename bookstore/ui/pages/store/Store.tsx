import React, { FunctionComponent, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { List, Typography } from "@mui/material";
import { getBooksRequest } from "@bookstore/ui/adapters/redux/books/books.actions";
import { selectBooks } from "@bookstore/ui/adapters/redux/books/books.selectors";
import Book from "./components/book/BookItem";
import Loader from "@core/ui/components/loader/Loader";
import Toast from "@core/ui/components/toast/Toast";

const Store: FunctionComponent = () => {
  const dispatch = useDispatch();
  const books = useSelector(selectBooks);

  useEffect(() => {
    dispatch(getBooksRequest());
  }, []);

  return (
    <>
      <Loader />
      <Toast />
      <Typography variant='h6' gutterBottom component='div'>
        Bookstore
      </Typography>
      <List sx={{ width: "100%", bgcolor: "background.paper" }}>
        {books.length ? (
          books.map((book) => <Book key={book.id} book={book} />)
        ) : (
          <Typography variant='body1' gutterBottom>
            You have deleted all books from the store! In order to salvage this situation, please use Postman to
            generate more books
          </Typography>
        )}
      </List>
    </>
  );
};

export default Store;
