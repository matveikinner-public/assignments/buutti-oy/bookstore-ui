import { Book } from "@bookstore/domain/models";
import CreateBook from "@bookstore/domain/models/createBook.model";
import UpdateBook from "@bookstore/domain/models/updateBook.model";
import {
  CREATE_BOOK_FAILURE,
  CREATE_BOOK_REQUEST,
  CREATE_BOOK_SUCCESS,
  DELETE_BOOK_FAILURE,
  DELETE_BOOK_REQUEST,
  DELETE_BOOK_SUCCESS,
  GET_BOOKS_FAILURE,
  GET_BOOKS_REQUEST,
  GET_BOOKS_SUCCESS,
  UPDATE_BOOK_FAILURE,
  UPDATE_BOOK_REQUEST,
  UPDATE_BOOK_SUCCESS,
} from "./books.contants";

export type BooksState = Book[];

export interface GetBooksRequest {
  type: typeof GET_BOOKS_REQUEST;
}

export interface GetBooksSuccess {
  type: typeof GET_BOOKS_SUCCESS;
  payload: Book[];
}

export interface GetBooksFailure {
  type: typeof GET_BOOKS_FAILURE;
}

export interface CreateBookRequest {
  type: typeof CREATE_BOOK_REQUEST;
  payload: CreateBook;
}

export interface CreateBookSuccess {
  type: typeof CREATE_BOOK_SUCCESS;
  payload: Book;
}

export interface CreateBookFailure {
  type: typeof CREATE_BOOK_FAILURE;
}

export interface UpdateBookRequest {
  type: typeof UPDATE_BOOK_REQUEST;
  payload: UpdateBook;
}

export interface UpdateBookSuccess {
  type: typeof UPDATE_BOOK_SUCCESS;
  payload: Book;
}

export interface UpdateBookFailure {
  type: typeof UPDATE_BOOK_FAILURE;
}

export interface DeleteBookRequest {
  type: typeof DELETE_BOOK_REQUEST;
  payload: string;
}

export interface DeleteBookSuccess {
  type: typeof DELETE_BOOK_SUCCESS;
  payload: string;
}

export interface DeleteBookFailure {
  type: typeof DELETE_BOOK_FAILURE;
}

export type BooksActionTypes =
  | GetBooksRequest
  | GetBooksSuccess
  | GetBooksFailure
  | CreateBookRequest
  | CreateBookSuccess
  | CreateBookFailure
  | UpdateBookRequest
  | UpdateBookSuccess
  | UpdateBookFailure
  | DeleteBookRequest
  | DeleteBookSuccess
  | DeleteBookFailure;
