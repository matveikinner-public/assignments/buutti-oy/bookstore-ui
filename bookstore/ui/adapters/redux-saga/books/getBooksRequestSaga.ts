import { put } from "redux-saga/effects";
import { createLoader, removeLoader } from "@core/ui/adapters/redux/loader/loader.actions";
import BOOK_BINDINDS from "@bookstore/di/book.bindings";
import booksContainer from "@bookstore/di/books.container";
import GetBooksUseCase from "@bookstore/domain/usecases/getBooksUseCase";
import { getBooksFailure, getBooksSuccess } from "../../redux/books/books.actions";
import { Book } from "@bookstore/domain/models";
import { Observable, lastValueFrom } from "rxjs";
import { createErrorToast, createSuccessToast } from "@core/ui/adapters/redux/toast/toast.actions";

export function* getBooksRequestSaga() {
  yield put(createLoader());
  const getBooksUseCase = booksContainer.get<GetBooksUseCase>(BOOK_BINDINDS.GetBooksUseCase);

  try {
    const books = (yield lastValueFrom((yield getBooksUseCase.invoke()) as Observable<Book[]>)) as Book[];
    yield put(getBooksSuccess(books));
    yield put(createSuccessToast("Success while attempting to fetch books"));
  } catch (err) {
    yield put(getBooksFailure());
    yield put(createErrorToast("Error while attempting to fetch books"));
  } finally {
    yield put(removeLoader());
  }
}
