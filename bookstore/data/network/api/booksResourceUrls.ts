namespace BOOKS_RESOURCE_URLS {
  export enum GET {}

  export enum POST {}

  export enum PUT {}

  export enum DELETE {}

  export enum CRUD {
    BOOKS = "api/v1/books",
  }
}

export default BOOKS_RESOURCE_URLS;
