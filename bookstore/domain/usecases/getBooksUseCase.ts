import { inject, injectable } from "inversify";
import BOOK_BINDINGS from "@bookstore/di/book.bindings";
import BooksRepository from "../repositories/booksRepository";

@injectable()
class GetBooksUseCase {
  @inject(BOOK_BINDINGS.BooksRepository) private booksRepository!: BooksRepository;

  invoke() {
    return this.booksRepository.getBooks();
  }
}

export default GetBooksUseCase;
